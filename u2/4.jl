### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 3fd0801c-ac64-11eb-08f0-659603b2aee3
using Plots

# ╔═╡ 07ee331f-f118-4d61-ba2e-9f373cc56cc0
function f_sup(a, b, c)
	d = b/2a
	e = c - a*(b/2a)^2
	g(x) = abs(a*(x-d)^2 + e)
	-1 <= d <= 1 ? max(g(-1), g(d), g(1)) : max(g(-1), g(1))
end

# ╔═╡ 711eff45-fcc1-4f76-b81f-85348d4fdb03
function plot_sup(;a, b, c)
	m = f_sup(a, b, c)
	p = plot(x->abs(a*x^2 + b*x + c), xlims=(-1, 1), label="")
	hline!(p, [m], label="")
	p
end

# ╔═╡ a04bdd0c-f69f-4fce-b6d5-2b038d52e1d6
plot_sup(a=-1, b=0, c=0.4)

# ╔═╡ ed9ea567-b9c4-4835-a88a-068a53fb8e06
plot_sup(a=-1, b=0, c=0.6)

# ╔═╡ b12fd01a-514e-4c13-a657-215ed465bf3a
plot_sup(a=-1, b=2, c=0.4)

# ╔═╡ d076a750-2c46-4821-8405-ae6c5e412b42
plot_sup(a=1, b=-1, c=0.4)

# ╔═╡ 40515777-d296-4918-af1c-9c817c3df6c1
plot_sup(a=1, b=4, c=3)

# ╔═╡ e66fec1b-8691-42b0-aa1a-c92e0c5be49b
plot_sup(a=-1, b=-5, c=10)

# ╔═╡ Cell order:
# ╠═3fd0801c-ac64-11eb-08f0-659603b2aee3
# ╠═07ee331f-f118-4d61-ba2e-9f373cc56cc0
# ╠═711eff45-fcc1-4f76-b81f-85348d4fdb03
# ╠═a04bdd0c-f69f-4fce-b6d5-2b038d52e1d6
# ╠═ed9ea567-b9c4-4835-a88a-068a53fb8e06
# ╠═b12fd01a-514e-4c13-a657-215ed465bf3a
# ╠═d076a750-2c46-4821-8405-ae6c5e412b42
# ╠═40515777-d296-4918-af1c-9c817c3df6c1
# ╠═e66fec1b-8691-42b0-aa1a-c92e0c5be49b
